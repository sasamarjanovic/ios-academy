//
//  ContentView.swift
//  Birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI

struct Tweet {
    let username: String
    var content: String
    let date: Date
}

struct ContentView: View {
    
    var tweet: Tweet = Tweet(username:
                                "salac", content: "voilm ieee", date: Date())
    
    var body: some View {
        VStack{
            HStack{
                Text("Birdy")
                    .font(.title)
                Spacer()
                Button(action: {}){
                    Text("Login")
                }
            }
            HStack{
                Image("bfb")
                    .resizable()
                    .frame(width: 55,height: 55)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                VStack{
                    Text(tweet.username)
                    Text(tweet.content)
                    Text(tweet.date, style: .relative)
                }
                .padding(.leading)
                Spacer()
            }
            Spacer()
           
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
